import dimscord, asyncdispatch, options, strutils, random, os, sequtils, math, tables, algorithm
import json

#dimscord: https://github.com/krisppurg/dimscord
#example of vote bot (in python): https://github.com/matnad/pollmaster/blob/0842b487ca29a84b226e1eeef5c868e9b239d325/models/poll.py

let discord = newDiscordClient("ODA0MDk5MzAxNzA1NTE1MDcw.YBHZ1A.cnWzdXRuytvPthzH-bcjgCQU7AI")
randomize()

type Vote = object
  rank: int
  userID: string
  choice: string #identify choices by emoji

type Poll = ref object
  title: string
  message: Message
  choices: seq[(string, string)]
  votes: seq[Vote]

var polls: seq[Poll] = @[]

proc pollFor(message: Message): Poll =
  for p in polls:
    if p.message.id == message.id:
      return p

proc getEmojiList(count: int): seq[string] =
  #🥇🥈🥉
  var list = @["🚀","🚠","🌈","🌜","🍉","🥐","🍔","🍟","🍕","🌭","🌮","🌯","🥙","🍿","🥗","🍄","🍗","🥓","🍆","🥑","🍇","🍬","🍩"]

  for i in 0..<count:
    let j = rand(0..list.len-1)
    result.add(list[j])
    list.delete(j)

proc createNewPoll(input: string): Poll =
  var poll = new(Poll)
  let choices = input.split(",")

  poll.title = choices[choices.low]

  let emojis = getEmojiList(choices.len-1)

  for i in 0..choices.len-2:
    let s = choices[i+1]
    poll.choices.add((emojis[i], s.strip))

  return poll

proc calculateWinner(poll: Poll): string =

  # a candidate must have this many votes to win
  let winCount = max((poll.votes.filter(proc(v: Vote): bool = v.rank == 0).len.toFloat * 0.5).ceil.toInt, 1)

  var choices = initTable[string, int]() # map emoji (choice) to a vote count
  for p in poll.choices: choices[p[0]] = 0
  for c in choices.keys: echo c

  for rank in 0..choices.len:
    for v in poll.votes:
      if v.rank <= rank:
        if v.choice.in(choices):
          choices[v.choice] += 1

    var winners: seq[string] = @[]
    for c in choices.keys:
      if choices[c] >= winCount:
        winners.add(c)

    if winners.len == 1: return winners[0]

    var lowestVote = high(int)
    for c in choices.keys:
      lowestVote = min(lowestVote, choices[c])

    # this deletes one of the lowest ranked choices
    # if there's a tie it deletes the first one it finds, but accounting for secondary votes would be better
    var losers: seq[string] = @[]
    for c in choices.keys:
      if choices[c] == lowestVote:
        losers.add(c)
        break

    for c in losers: choices.del(c)

    #reset vote counts
    for c in choices.keys:
      choices[c] = 0

  return ""


proc generatePollText(poll: Poll): string =
  let w = calculateWinner(poll)
  echo "current winner is: " & w

  for c in poll.choices:
    result.add(c[0] & " " & c[1])
    if c[0] == w:
      result.add(" ✓")
    result.add("\n")

  result.add("\n")

  result.add("""Click the emoji to vote. Vote for as many as you like (the first one you click will be your first choice, the next will be your second, and so on). 💥 will reset your votes.""")

proc rerankVotes(poll: var Poll, userID: string) =

  var userVotes = poll.votes.filter(proc(v: Vote): bool = v.userID == userID)
  userVotes.sort(proc(x, y: Vote): int = 
    if x.rank < y.rank: return -1
    if x.rank == y.rank: return 0
    else: return 1)

  var newVotes: seq[Vote] = @[]

  var i = 0
  for v in userVotes:
    newVotes.add(Vote(userID: v.userID, choice: v.choice, rank: i))
    i += 1

  poll.votes.keepIf(proc(v: Vote): bool = not (v.userID == userID))
  poll.votes.add(newVotes)

proc saveVote(poll: var Poll, userID: string, choice: string) =
  let c = poll.votes.filter(proc(v: Vote): bool = v.userID == userID).len
  let v = Vote(userID: userID, choice: choice, rank: c)
  poll.votes.add(v)

proc removeVote(poll: var Poll, userID: string, choice: string) =
  poll.votes.keepIf(proc(v: Vote): bool = not (v.userID == userID and v.choice == choice))
  rerankVotes(poll, userID)

proc removeAllVotesBy(poll: var Poll, userID: string) =
  poll.votes.keepIf(proc(v: Vote): bool = v.userID != userID)

proc debugVotes(poll: Poll) =
  for v in poll.votes:
    echo v.userID & " voted for " & v.choice & " rank: " & $v.rank


## EVENT HANDLING
proc onReady(s: Shard, r: Ready) {.event(discord).} =
    echo "Ready as " & $r.user

proc messageCreate(s: Shard, m: Message) {.event(discord).} =
    if m.author.bot: return
    if m.content.find("!poll")  == 0:

      var poll = createNewPoll(m.content.replace("!poll").strip)

      if poll.choices.len > 1:
        poll.message = await discord.api.sendMessage(m.channel_id,
                                                     embeds = @[Embed(
                                                         title: some poll.title,
                                                         description: some generatePollText(poll),
                                                     )]
                                                 )

        polls.add(poll)

        for c in poll.choices:
          discard discord.api.addMessageReaction(poll.message.channel_id, poll.message.id, c[0])
        discard discord.api.addMessageReaction(poll.message.channel_id, poll.message.id, "💥")

      else:
        asyncCheck discord.api.sendMessage(
            m.channel_id,
            "To create a poll, use the format: !poll Title of Poll, Option 1, Option 2, Option 3"
        )


proc messageReactionAdd(s: Shard, m: Message, u: User, e: Emoji, exists: bool) {.event(discord).} =
  echo "reaction added by " & u.id & $e.name

  if u.bot: return

  echo "reaction added by " & u.id & $e.name
  var poll = pollFor(m)
  if poll != nil:
    if e.name.get().in(poll.choices.map(proc(p: (string, string)): string = return p[0])):
      # don't save a vote if the emoji isn't in our list of choices
      saveVote(poll, u.id, e.name.get())

    elif e.name.get() == "💥":

      var userVotes = poll.votes.filter(proc(v: Vote): bool = v.userID == u.id)
      var emoji = userVotes.map(proc(v: Vote): string = v.choice)
      emoji.add("💥")

      removeAllVotesBy(poll, u.id)

      for c in emoji:
        asyncCheck discord.api.deleteMessageReaction(poll.message.channel_id, poll.message.id, c, u.id)

    asyncCheck discord.api.editMessage(poll.message.channel_id, poll.message.id, embeds = @[Embed(
      title: some poll.title, 
      description: some generatePollText(poll),
    )])

    debugVotes(poll)
  else:
    echo "poll not found"

proc messageReactionRemove(s: Shard, m: Message, u: User, r: Reaction, exists: bool) {.event(discord).} =
  echo "reaction removed by " & u.id

  var poll = pollFor(m)
  if poll != nil:
    removeVote(poll, u.id, r.emoji.name.get())

    asyncCheck discord.api.editMessage(poll.message.channel_id, poll.message.id, embeds = @[Embed(
      title: some poll.title, 
      description: some generatePollText(poll),
    )])
  else:
    echo "poll not found"

waitFor discord.startSession()
